# Golang Hello World App

Credits @Panzouh

## Run

```sh
git clone https://gitlab.com/panzouh/go-hello-world
cd go-hello-world; go run main.go
```
