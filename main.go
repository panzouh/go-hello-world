package main

import (
	"log"
	"net/http"
	"text/template"
)

var listenPort = ":8080"

const tmpl = `
<h1>HELLO FROM CONTAINER</h1>
<img src="https://data.photofunky.net/output/image/b/b/4/e/bb4e5d/photofunky.gif" alt="Gatsby" width="500" height="200" />`

func main() {
	http.HandleFunc("/", index)
	log.Printf("Started listening on %s", listenPort)
	http.ListenAndServe(listenPort, nil)
}

func index(w http.ResponseWriter, r *http.Request) {
	t, err := template.New("webpage").Parse(tmpl)
	if err != nil {
		log.Fatal(err)
	}
	t.ExecuteTemplate(w, "webpage", nil)
}
